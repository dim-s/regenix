<?php

namespace {

    use regenix\AbstractGlobalBootstrap;
    use regenix\mvc\Controller;
    use regenix\mvc\Request;

    class GlobalBootstrap extends AbstractGlobalBootstrap {

        // override any methods ..
    }

}